Software
========

The code is written for use on an Arduino Uno (or compatible) microcontroller,
but should run on other boards subject to modifications. It uses the
`IOAbstraction
<https://www.thecoderscorner.com/products/arduino-libraries/io-abstraction/>`_,
`tcMenu <https://www.thecoderscorner.com/products/arduino-libraries/tc-menu/>`_
and `LiquidCrystalIO  <https://github.com/davetcc/LiquidCrystalIO>`_ libraries.

Compile and upload using the Arduino software in the usual manner.
