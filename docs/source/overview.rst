Overview
========

Here's some photographs of the prototype in operation. Selecting the ``Spin`` menu item starts or stops the fan rotation.

.. image:: images/spincoater-off.jpg

.. image:: images/spincoater-on.jpg

The ``RPM`` line displays the currently measured rotation speed, in revolutions per minute.
Selecting ``Set RPM`` allows the desired rotation speed to be adjusted with the up and down buttons.

.. image:: images/spincoater-setrpm.jpg

There is an optional timer which will automatically stop the spinning after a set duration. Set the timer to 0 to spin indefinitely.

.. image:: images/spincoater-settimer.jpg

The target RPM speed is achieved using a calibration table. Changing the power
supply voltage, fan type, load etc will require the calibration table to be
re-built. If the measured RPM is not matching the set RPM, run the calibration
procedure. It should ramp up the spin speed, and complete in 1--2 minutes.

.. image:: images/spincoater-calibration.jpg

.. image:: images/spincoater-calibrationmenu.jpg

.. image:: images/spincoater-calibrationstart.jpg

.. image:: images/spincoater-calibrating.jpg

The currently set RPM and timer values can be saved to the device to use as
defaults for initial power on. Note that the calibration table is automatically
saved on completion.

.. image:: images/spincoater-savedefaults.jpg

.. image:: images/spincoater-defaultssaved.jpg
