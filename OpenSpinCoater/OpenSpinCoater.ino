#include "OpenSpinCoater_menu.h"

// Enable serial port debug messages
// #define DEBUG

#define PIN_RPM_SENSE 2 // Pin for sensing RPM pulses
#define TCCRB TCCR2B    // Timer control register B (timer2)
#define PIN_PWM_OUT 11  // a PWM pin for above timer#
#define PIN_START 3     // a PWM pin for above timer#

// State machine modes
#define MODE_IDLE 0
#define MODE_CALIBRATION 1
#define MODE_CALIBRATION_CONFIGURE 2
#define MODE_CALIBRATION_STABILISE 3
#define MODE_CALIBRATION_MEASURE 4
#define MODE_CALIBRATION_CANCEL 5
#define MODE_CALIBRATION_COMPLETE 6
int current_mode = MODE_IDLE;


// IOAbstraction interface to EEPROM
AvrEeprom eeprom;

// Lookup table of PWM to RPM
#define CALIBRATION_TABLE_LOC 800    // Location in EEPROM
#define CALIBRATION_TABLE_LENGTH 10  // Number of entries
typedef struct table_entry_struct {
  byte pwm;
  unsigned int rpm;
} TableEntry;
TableEntry table[CALIBRATION_TABLE_LENGTH];

// Temporary table and config used during calibration
byte calibration_table_i = 0;
byte calibration_pwm = 0;
unsigned long calibration_millis_prev = 0;
TableEntry table_temp[CALIBRATION_TABLE_LENGTH];

// Track state of LED
int led_state = 0;

// Menu dialog
BaseDialog* dlg = renderer.getDialog();

// Handle for RPM update scheduler task
byte task_rpm;

// Handler for RPM pin interrupt
volatile unsigned long pulse_count = 0;
unsigned long rpm_prev_millis = 0;
void rpm_pulse() {
  pulse_count++;
  digitalWrite(LED_BUILTIN, led_state = !led_state);
}


// Calculate and return RPM from counted pulses and time interval
unsigned int get_rpm() {
  noInterrupts();
  // two pulses per revolution?
  unsigned int rpm = (30000*pulse_count/(millis() - rpm_prev_millis));
  pulse_count = 0;
  rpm_prev_millis = millis();
  interrupts();
  return rpm;
}

void update_rpm() {
  unsigned int rpm = get_rpm();
  #ifdef DEBUG      
  Serial.println("Read RPM = " + String(rpm));
  #endif //DEBUG
  menuRPM.setCurrentValue(rpm);
}

// State machine state enter/leave routines
void switch_mode(int mode) {
  current_mode = mode;
  enter_mode(mode);
}


void enter_mode(int mode) {
  switch (mode) {
    case MODE_IDLE:
      #ifdef DEBUG
      Serial.println("Idle.");
      #endif //DEBUG
      // Start scheduling updating RPM status updates
      task_rpm = taskManager.scheduleFixedRate(2000, update_rpm);
      break;
    case MODE_CALIBRATION: 
      #ifdef DEBUG
      Serial.println(F("Starting calibration..."));
      #endif //DEBUG
      // Stop updating RPM
      taskManager.cancelTask(task_rpm);
      // Ensure spin is enabled
      digitalWrite(PIN_START, HIGH);
      analogWrite(PIN_PWM_OUT, 0);
      calibration_table_i = 0;
      switch_mode(MODE_CALIBRATION_CONFIGURE);
      break;
    case MODE_CALIBRATION_CONFIGURE:
      if (calibration_table_i < CALIBRATION_TABLE_LENGTH) {
        // Quadratic spacing so more detail at lower PWM values
        calibration_pwm = byte(255*pow(float(calibration_table_i)/(CALIBRATION_TABLE_LENGTH - 1), 2));
        #ifdef DEBUG
        Serial.print(String(calibration_table_i) + ", " + String(calibration_pwm) + ", ");
        #endif //DEBUG
        analogWrite(PIN_PWM_OUT, calibration_pwm);
        switch_mode(MODE_CALIBRATION_STABILISE);
      } else {
        dlg->hide();
        switch_mode(MODE_CALIBRATION_COMPLETE); 
      }
      break;
    case MODE_CALIBRATION_MEASURE:
      get_rpm();
      calibration_millis_prev = millis();
      break;
    case MODE_CALIBRATION_STABILISE:
      calibration_millis_prev = millis();
      break;
    case MODE_CALIBRATION_CANCEL:
      #ifdef DEBUG
      Serial.println(F("Stopping calibration."));
      #endif //DEBUG
      digitalWrite(PIN_START, LOW);
      onRPM(0);
      switch_mode(MODE_IDLE);
      break;
    case MODE_CALIBRATION_COMPLETE:
      #ifdef DEBUG
      Serial.println(F("Calibration complete."));
      #endif //DEBUG
      digitalWrite(PIN_START, LOW);
      // Copy temporary table to main, write to EEPROM
      memcpy(table, table_temp, CALIBRATION_TABLE_LENGTH*sizeof(TableEntry));
      eeprom.write16(CALIBRATION_TABLE_LOC, 0xbeef);
      eeprom.writeArrayToRom(CALIBRATION_TABLE_LOC + 2, (byte*)table, CALIBRATION_TABLE_LENGTH*sizeof(TableEntry));
      onRPM(0); // Note 0 is id, not requested RPM...
      switch_mode(MODE_IDLE);
  }
}

void setup() {
  // Serial port interface for status report and control
  #ifdef DEBUG
  Serial.begin(115200);
  #endif //DEBUG
  
  // Configure timer for fast PWM
  // Choose 31 KHz as it's above audio frequency range
  // 4-pin fan spec is 25 KHz, but we can only get that with the 16-bit timer1,
  // and the LCD brightness is connected to pin 10. This should do.
  TCCRB = TCCRB & B11111000 | B00000001; // No prescaling (31 KHz)
  analogWrite(PIN_PWM_OUT, 0);

  // Input pin for RPM pulse counting
  pinMode(PIN_RPM_SENSE, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PIN_RPM_SENSE), rpm_pulse, RISING);

  // Pin to switch 12 V output
  pinMode(PIN_START, OUTPUT);
  // TODO control drive voltage with PWM analog output
  // analogWrite(PIN_START, 0);

  // Built in LED for some feedback
  pinMode(LED_BUILTIN, OUTPUT);

  // Load PWM-to-RPM table from EEPROM
  // Check presence of magic bytes (a CRC would be better, but this should be fine)
  if (eeprom.read16(CALIBRATION_TABLE_LOC) == 0xbeef) {
    // Magic bytes OK, table should be valid
    eeprom.readIntoMemArray((byte*)table, CALIBRATION_TABLE_LOC + 2, CALIBRATION_TABLE_LENGTH*sizeof(TableEntry));
    #ifdef DEBUG
    Serial.println(F("Calibration table read from EERPOM:"));
    for (byte i = 0; i < CALIBRATION_TABLE_LENGTH; i++) {
      Serial.println(String(i) + ", " + String(table[i].pwm) + ", " + String(table[i].rpm));
    }
    #endif //DEBUG
  } else {
    // Magic bytes don't match, table invalid, build a rough one
    #ifdef DEBUG
    Serial.println(F("Calibration table in EEPROM is invalid, using default:"));
    #endif //DEBUG
    for (byte i = 0; i < CALIBRATION_TABLE_LENGTH; i++) {
      byte pwm = i * byte(256/CALIBRATION_TABLE_LENGTH);
      table[i] = {byte(pwm), (unsigned int)(10*pwm)};
      #ifdef DEBUG
      Serial.println(String(table[i].pwm) + ", " + String(table[i].rpm));
      #endif //DEBUG
    }
  }
  
  setupMenu();
  renderer.setTitleRequired(false);
  menuMgr.load(eeprom);

  switch_mode(MODE_IDLE);
}


void loop() {
  // Run UI/event loop
  taskManager.runLoop();
  
  // Select action for this loop cycle depending on current mode
  switch (current_mode) {
    case MODE_IDLE:
      break;
    case MODE_CALIBRATION_STABILISE:
      // New PWM set, waiting to stabilise
      if (millis() - calibration_millis_prev > 3000)
        switch_mode(MODE_CALIBRATION_MEASURE);
      break;
    case MODE_CALIBRATION_MEASURE:
      // Waiting for measurement period to end
      if (millis() - calibration_millis_prev > 5000) {
        table_temp[calibration_table_i].pwm = calibration_pwm;
        table_temp[calibration_table_i].rpm = get_rpm();
        #ifdef DEBUG
        Serial.println(table_temp[calibration_table_i].rpm);
        #endif //DEBUG
        calibration_table_i++;
        switch_mode(MODE_CALIBRATION_CONFIGURE);
      }
      break;
  }
}


void CALLBACK_FUNCTION onRPM(int id) {
  unsigned int rpm = 100*(menuSetRPM.getOffset() + menuSetRPM.getCurrentValue());
  byte pwm = 0;
  byte i = 0;
  for (i = 0; i < CALIBRATION_TABLE_LENGTH; i++) {
    if (table[i].rpm > rpm) break;
  }
  if (i == 0) {
    // Requested RPM is lower than min achievable
    pwm = 0;
  }else if (i >= CALIBRATION_TABLE_LENGTH) {
    // Requested RPM is greater than max achievable
    pwm = 255;
  } else {
    // Interpolate
    int dx = table[i].pwm - table[i - 1].pwm;
    int dy = table[i].rpm - table[i - 1].rpm;
    pwm = table[i - 1].pwm + int((dx*(rpm - table[i - 1].rpm))/dy);
  }
  #ifdef DEBUG
  Serial.println("Setting RPM = " + String(rpm) + ", PWM = " + String(pwm));
  #endif //DEBUG
  analogWrite(PIN_PWM_OUT, pwm);
}

// Stop spinning after timer elapsed
void stopSpin() {
  menuSpin.setBoolean(false);
}

void CALLBACK_FUNCTION onSpin(int id) {
  #ifdef DEBUG
  Serial.println("Spin = " + String(menuSpin.getCurrentValue()));
  #endif //DEBUG
  digitalWrite(PIN_START, menuSpin.getCurrentValue());
  // TODO instead, control drive voltage with analog output (PWM) on this pin
  // analogWrite(PIN_START, menuSpin.getCurrentValue()*25);
  if (menuSpin.getCurrentValue() && menuSetTimer.getCurrentValue() > 0) {
    // If the timer is enabed, stop again after specified time
    taskManager.scheduleOnce(1000*menuSetTimer.getCurrentValue(), stopSpin);
  }
}


void onDialogFinished(ButtonType btnPressed, void* /*userdata*/) {
  #ifdef DEBUG      
  Serial.println(F("Calibration cancel selected."));
  #endif //DEBUG
  switch_mode(MODE_CALIBRATION_CANCEL);
}


// The header just gets overwritten by buffer on 2-line LCD anyway...
const char MESSAGE_CALIBRATING[] PROGMEM = "";
void CALLBACK_FUNCTION onStart(int id) {
  #ifdef DEBUG
  Serial.println(F("Calibration start selected."));
  #endif //DEBUG
  if (current_mode == MODE_IDLE) {
    dlg->setButtons(BTNTYPE_NONE, BTNTYPE_CANCEL, 1);
    dlg->show(MESSAGE_CALIBRATING, true, onDialogFinished);
    dlg->copyIntoBuffer("Calibrating...");
    switch_mode(MODE_CALIBRATION);
  }
}


void CALLBACK_FUNCTION onTimerChange(int id) {
    #ifdef DEBUG
    Serial.println("Timer change: " + String(menuSetTimer.getCurrentValue()));
    #endif //DEBUG
    // TODO what to do when timer value changed while spinning etc?
}


void CALLBACK_FUNCTION onSaveDefaults(int id) {
    #ifdef DEBUG
    Serial.println(F("Save defaults selected."));
    #endif //DEBUG
    // Save current RPM, timer settings to EEPROM to restore on reset
    menuMgr.save(eeprom);
    // Show some feedback to user
    dlg->setButtons(BTNTYPE_NONE, BTNTYPE_OK, 1);
    dlg->show(MESSAGE_CALIBRATING, true);
    dlg->copyIntoBuffer("Defaults saved.");
}
