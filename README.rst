OpenSpinCoater --- A Free Spin Coater Design
============================================

OpenSpinCoater is a design for a simple `spin coating machine
<https://en.wikipedia.org/wiki/Spin_coating>`_, built using inexpensive parts.
The microcontroller is an Arduino Uno, and the motor is a 12 volt, 4-pin
computer fan. It should be able to be constructed for around the AUD$50 mark,
well below that of `commercial devices
<https://www.ossila.com/products/spin-coater>`_.

Source code: https://gitlab.com/ptapping/openspincoater

Documentation: https://openspincoater.readthedocs.io/
